#include "twitter.h"

#include <openssl/bio.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/engine.h>
#include <openssl/rand.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <ctype.h>
#include <inttypes.h>

static const size_t buf_size = 1024;

/* Init all necessary openssl modules. */
void openssl_init(void)
{
    CRYPTO_malloc_init();
    SSL_library_init();
    SSL_load_error_strings();
    ERR_load_BIO_strings();
    OpenSSL_add_all_algorithms();
    ENGINE_load_builtin_engines();
    ENGINE_register_all_complete();
}

/**
 * Posts given POST to api.twitter.com.
 *
 * From https://thunked.org/programming/openssl-tutorial-client-t11.html and
 * example in https://www.openssl.org/docs/crypto/BIO_f_ssl.html.
 *
 * This doesn't verify the server certificate, i.e. it will accept certificates
 * signed by any CA.
 */
static int send_to_twitter(const char *post)
{
    SSL_CTX* ctx = SSL_CTX_new(SSLv23_client_method());
    if (ctx == NULL) {
        printf("Error creating SSL_CTX\n");
        return -1;
    }

    BIO* bio = BIO_new_ssl_connect(ctx);
    if (bio == NULL) {
        printf("Error creating BIO!\n");
        SSL_CTX_free(ctx);
        return -1;
    }

    SSL* ssl;
    BIO_get_ssl(bio, &ssl);
    if (ssl == NULL) {
        printf("BIO_get_ssl failed\n");
        BIO_free_all(bio);
        SSL_CTX_free(ctx);
        return -1;
    }

    SSL_set_mode(ssl, SSL_MODE_AUTO_RETRY);
    BIO_set_conn_hostname(bio, "api.twitter.com:https");

    if (BIO_do_connect(bio) <= 0) {
        printf("Failed to connect!");
        BIO_free_all(bio);
        SSL_CTX_free(ctx);
        return -1;
    }

    if (BIO_do_handshake(bio) <= 0) {
        printf("Failed to do SSL handshake!");
        BIO_free_all(bio);
        SSL_CTX_free(ctx);
        return -1;
    }

    int ret;

    char buf[1024] = { 0 };
    BIO_puts(bio, post);

    int read_bytes = BIO_read(bio, buf, sizeof(buf) - 1);
    if (read_bytes > 0) {
        buf[read_bytes] = '\0';

        if (strstr(buf, "HTTP/1.1 200 OK") != NULL) {
            ret = 0;
        } else if (strstr(buf, "HTTP/1.1 403 Forbidden") != NULL) {
            /* Twitter doesn't allow consecutive duplicates and will respond
             * with 403 in such case. */
            printf("Twitter responded with 403!\n");
            ret = -2;
        } else {
            printf("Error occurred! Received:\n"
                   "%s\n", buf);
            ret = -1;
        }
    } else {
        printf("Read failed!\n");
        ret = -1;
    }

    BIO_free_all(bio);
    SSL_CTX_free(ctx);

    return ret;
}

/* Base64 encode the given string. */
static char *base64_encode(const char *msg, size_t msg_len)
{
    /* This could be improved. Currently it allocates bit too much. Times three
     * is required for the percent encoding but times four is just overestimate
     * of base64 encoding. */
    size_t signature_len = msg_len * 4 * 3 + 1;

    char *encoded = malloc(signature_len);
    if (encoded == NULL) {
        return NULL;
    }

    /* From https://www.openssl.org/docs/crypto/BIO_f_base64.html */
    BIO *b64 = BIO_new(BIO_f_base64());
    if (b64 == NULL) {
        free(encoded);
        return NULL;
    }

    /* BIO base64 doesn't support BIO_gets() or _puts but writes to a stream, so
     * open a stream to the buffer. */
    FILE *stream = fmemopen(encoded, signature_len, "w");
    if (stream == NULL) {
        free(encoded);
        BIO_free_all(b64);
        return NULL;
    }

    BIO *bio = BIO_new_fp(stream, BIO_NOCLOSE);
    BIO_push(b64, bio);
    BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);
    BIO_write(b64, msg, (int)msg_len);
    BIO_flush(b64);

    BIO_free_all(b64);
    fclose(stream);

    return encoded;
}

/* Generate nonce used as oauth_nonce. */
static char *generate_nonce(void)
{
    char nonce[32] = {0};

    if (RAND_bytes((unsigned char *)nonce, sizeof(nonce)) != 1) {
        return NULL;
    }

    char *encoded_nonce = base64_encode(nonce, sizeof(nonce));
    if (encoded_nonce == NULL) {
        return NULL;
    }

    size_t nonce_len = strlen(encoded_nonce);
    for (size_t i = 0; i < nonce_len; i++) {
        if (!isalnum((int)encoded_nonce[i])) {
            /* Replace non-alphanumerics by arbitrary 'a' since they should not
             * be present in the nonce. */
            encoded_nonce[i] = 'a';
        }
    }

    return encoded_nonce;
}

/* Does character c need percent encoding or is it a unreserved character? */
static int need_percent_encoding(char c)
{
    return !(isalnum((int)c) || c == '-' || c == '.' || c == '_' || c == '~');

}

/* Percent encode (or URL encode) given string. There should be enough room in
 * the str (three times uncoded charactes). */
static char *percent_encode(char *str)
{
    /* Length has to be updated on each iteration. */
    for (unsigned int i = 0; str[i] != '\0'; i++) {
        if (need_percent_encoding(str[i])) {
            /* Make room for two characters. */
            memmove(str + i + 3, str + i + 1, strlen(str) - i + 1);
            /* Write '%' and two bytes of which the character consists of. */
            char tmp[2];
            snprintf(tmp, 2, "%X", str[i] >> 4);
            str[i + 1] = tmp[0];
            snprintf(tmp, 2, "%X", str[i] & 0xf);
            str[i + 2] = tmp[0];
            str[i] = '%';
        }
    }

    return str;
}

static char *create_post(const char *timestamp, const char *nonce, const char *status,
                         const char *signature, const char *consumer_key,
                         const char *auth_token)
{
    char *post = malloc(buf_size);
    if (post == NULL) {
        return NULL;
    }

    int ret = snprintf(post, buf_size, "POST /1.1/statuses/update.json HTTP/1.1\r\n"
                       "User-Agent: LightBot\r\n"
                       "Host: api.twitter.com\r\n"
                       "Accept: */*\r\n"
                       "Content-Type: application/x-www-form-urlencoded\r\n"
                       "Authorization: OAuth oauth_consumer_key=\"%s\", oauth_nonce=\"%s\", "
                       "oauth_signature=\"%s\", oauth_signature_method=\"HMAC-SHA1\", "
                       "oauth_timestamp=\"%s\", oauth_token=\"%s\", oauth_version=\"1.0\"\r\n"
                       "Content-Length: %zu\r\n\r\n"
                       "status=%s",
                       consumer_key, nonce, signature, timestamp, auth_token,
                       strlen("status=") + strlen(status), status);
    if (ret < 0 || (size_t)ret >= buf_size) {
        free(post);
        return NULL;
    }

    return post;
}

static unsigned char *hmac_sha1_encode(const char *data, const char *hmac_key,
                                       unsigned int *result_len)
{
    *result_len = 20; /* Should be always 20 bytes. */
    unsigned char *result = malloc(*result_len);
    if (result == NULL) {
        return NULL;
    }

    HMAC_CTX ctx;

    /* Example from http://stackoverflow.com/a/245335. */
    HMAC_CTX_init(&ctx);
    HMAC_Init_ex(&ctx, hmac_key, (int)strlen(hmac_key), EVP_sha1(), NULL);
    HMAC_Update(&ctx, (const unsigned char *)data, strlen(data));
    HMAC_Final(&ctx, result, result_len);
    HMAC_CTX_cleanup(&ctx);

    return result;
}

/* Compute the signature from given parameters as required by the OAuth. */
static char *compute_signature(const char *timestamp, const char *nonce, const char *status,
                               const char *consumer_key, const char *auth_token,
                               const char *hmac_key)
{
    char *signature_base = malloc(buf_size);
    if (signature_base == NULL) {
        return NULL;
    }

    /* Encode the status again. */
    size_t encoded_size = strlen(status) * 3 + 1;
    char *encoded_status = malloc(encoded_size);
    if (encoded_status == NULL) {
        free(signature_base);
        return NULL;
    }

    snprintf(encoded_status, encoded_size, "%s", status);
    percent_encode(encoded_status);

    int ret = snprintf(signature_base, buf_size, "POST&https%%3A%%2F%%2Fapi.twitter.com"
                       "%%2F1.1%%2Fstatuses%%2Fupdate.json&"
                       "oauth_consumer_key%%3D%s%%26oauth_nonce%%3D%s"
                       "%%26oauth_signature_method%%3DHMAC-SHA1"
                       "%%26oauth_timestamp%%3D%s%%26oauth_token%%3D%s"
                       "%%26oauth_version%%3D1.0"
                       "%%26status%%3D%s",
                       consumer_key, nonce, timestamp, auth_token, encoded_status);
    free(encoded_status);
    if (ret < 0 || (size_t)ret > buf_size) {
        free(signature_base);
        return NULL;
    }

    unsigned int encoded_len;
    char *hmac_encoded = (char *)hmac_sha1_encode(signature_base, hmac_key,
                                                  &encoded_len);
    free(signature_base);

    if (hmac_encoded == NULL) {
        return NULL;
    }

    char *encoded = base64_encode(hmac_encoded, encoded_len);
    free(hmac_encoded);

    if (encoded == NULL) {
        return NULL;
    }

    return percent_encode(encoded);
}

int send_update_to_twitter(const char *message, const char *consumer_key,
                           const char *auth_token, const char *hmac_key)
{
    char status[141];

    snprintf(status, sizeof(status), "%s", message);

    percent_encode(status);

    char *nonce = generate_nonce();
    if (nonce == NULL) {
        return -1;
    }

    time_t t = time(NULL);
    char timestamp[11];
    snprintf(timestamp, sizeof(timestamp), "%zu", (size_t)t);

    char *sig = compute_signature(timestamp, nonce, status,
                                  consumer_key, auth_token, hmac_key);
    if (sig == NULL) {
        free(nonce);
        return -1;
    }

    char *post = create_post(timestamp, nonce, status, sig,
                             consumer_key, auth_token);
    free(sig);
    free(nonce);

    if (post == NULL) {
        return -1;
    }

    int ret = send_to_twitter(post);
    free(post);

    return ret;
}
