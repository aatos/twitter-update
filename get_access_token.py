#!/usr/bin/env python3

import tweepy

def get_consumer_keys():
    consumer_key = input("Consumer key: ")
    consumer_secret = input("Consumer secret: ")

    return (consumer_key, consumer_secret)


def get_access_tokens(consumer_key, consumer_secret):
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    url = auth.get_authorization_url()

    pin = input("Login in {}\nand type in the PIN here: ".format(url))
    token, secret = auth.get_access_token(pin)

    print("Access Token '{}',\nAccess Token Secret '{}'".format(token, secret))

if __name__ == "__main__":
    key, secret = get_consumer_keys()
    get_access_tokens(key, secret)
