# Cygwin doesn't define fmemopen(3) if ansi-c is defined, so use gnu
CFLAGS = -std=gnu11
CFLAGS += -pedantic -Wall -Wextra
CFLAGS += -Wformat=2 -Wswitch-default -Wcast-align -Wpointer-arith -Wnested-externs
CFLAGS += -Wbad-function-cast -Wstrict-prototypes -Winline -Wundef -Wcast-qual
CFLAGS += -Wshadow -Wwrite-strings -Wconversion -Wstrict-aliasing=2 -Wunused-result

CFLAGS += -D_GNU_SOURCE # To enable fmemopen(3) on Ubuntu 12.04

LIBS = -lssl -lcrypto
SRCS = twitter.c twitter_main.c

.PHONY: all

all: twitter

twitter: $(SRCS)
	$(CC) $^ $(CFLAGS) -o $@ $(LIBS)

.PHONY: clean

clean:
	rm -f *.o *.a twitter *~
