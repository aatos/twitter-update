#ifndef TWITTER_H
#define TWITTER_H

void openssl_init(void);

int send_update_to_twitter(const char *message, const char *consumer_key,
                           const char *auth_token, const char *hmac_key);

#endif
