#include "twitter.h"

#include <stdio.h>

int main(int argc, const char *argv[])
{
    if (argc != 5) {
        fprintf(stderr, "Usage: %s <message> <consumer_key> <auth_token> <hmac_key>\n",
                argv[0]);
        return -1;
    }

    openssl_init();

    int ret = send_update_to_twitter(argv[1], argv[2], argv[3], argv[4]);
    printf("sending to twitter %s: %d\n",
           ret == 0 ? "succeeded" : "failed", ret);

    return 0;
}
